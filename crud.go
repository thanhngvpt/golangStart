package main

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

// db handler
func initingDb(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		log.Fatal(err)
	}

	if db == nil {
		panic(err)
	}

	// return db has type *sql.DB
	return db
}

func createTable(db *sql.DB) {
	sqlStr := `
		CREATE TABLE IF NOT EXISTS monthy(
			id INTEGER PRIMARY KEY AUTOINCREMENT,
			electric_amount INTEGER,
			water_amount INTEGER,
			monthy TEXT,
			created TEXT,
			room_money REAL,
			electric_money REAL,
			water_money REAL,
			other_money REAL,
			total_money REAL
		)
	`
	_, err := db.Exec(sqlStr)
	if err != nil {
		log.Fatal(err)
	}
}

func create(db *sql.DB, spends []Spends) {
	sqlInsert := `
		INSERT OR REPLACE INTO monthy(id, electric_amount, water_amount, monthy, created, electric, water, total)
		VALUES(?, ?, ?, ?, ?, ?, ?, ?)
	`
	smtp, err := db.Prepare(sqlInsert)
	if err != nil {
		log.Fatal(err)
	}
	defer smtp.Close()


}
