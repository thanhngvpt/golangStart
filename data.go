package main

type Spend struct {
	Id              int     `json:"id"`
	Electric_amount int     `json:"electrict_amount"`
	Water_amount    int     `json:"water_amount"`
	Monthy          string  `json:"monthy"`
	Created         string  `json:"created"`
	Electric        float64 `json:"electric"`
	Water           float64 `json:"water"`
	Total           float64 `json:"total"`
}

type Spends []Spend