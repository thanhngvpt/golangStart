package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/http"
	"os"
)

var db *sqlx.DB

type Spend struct {
	Id             int    `db:"id"`
	ElectricAmount int    `db:"electric_amount"`
	WaterAmount    int    `db:"water_amount"`
	Monthy         string `db:"monthy"`
	Created        string `db:"created"`
	TotalMoney     int    `db:"total_money"`
}

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "8989"
	}

	// router
	router := gin.New()
	router.Use(gin.Logger())
	router.LoadHTMLGlob("templates/*.tmpl.html")
	router.Static("/static", "static")

	// API
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl.html", nil)
	})

	// DB
	db, _ = sqlx.Open("sqlite3", "db.sqlite3")
	defer db.Close()
	err := db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	//rows, err := db.Queryx("SELECT * FROM monthy_spend")
	//for rows.Next() {
	//	var s Spend
	//	err := rows.StructScan(&s)
	//	if err != nil {
	//		log.Fatal(err)
	//	}
	//
	//	fmt.Printf("%p\n", s)
	//}

	row := db.QueryRowx("SELECT * FROM monthy_spend")
	var s Spend
	err = row.StructScan(&s)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(s)

	// run webserver
	router.Run(":" + port)
}
