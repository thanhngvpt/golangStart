package main

import (
	"fmt"
	"bufio"
	"os"
	"strings"
)

func sliceContain(sl []string, s string) bool {
	count := 0
	for _, e := range sl {
		if e == s {
			count++
		}
	}

	if count == 1 {
		return true
	}

	return false
}

func isSliceDuplicate(sl []string) bool {
	encountered := map[string]bool{}
	for v := range sl {
		if _, ok := encountered[sl[v]]; !ok {
			encountered[sl[v]] = true
		} else {
			return true
		}
	}

	return false
}

func main() {
	var l1 string
	fmt.Scanln(&l1)

	reader := bufio.NewReader(os.Stdin)
	text, _, _:= reader.ReadLine()

	l2 := strings.Fields(string(text))

	if sliceContain(l2, l1) && !isSliceDuplicate(l2) {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}

}