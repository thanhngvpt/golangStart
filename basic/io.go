package main

import (
	"fmt"
	"strconv"
	"log"
	"math"
)

// functions
func f1(x float64) float64 {
	return math.Mod(x, 11)
}

func f2(x float64) float64 {
	return x * x
}

// main
func main() {
	// f1
	var input string
	fmt.Scanln(&input)

	number, err := strconv.ParseFloat(input, 64)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(math.Mod(number, 11))

	// f2
	fmt.Println(f2(2))
}