package main

import (
	"database/sql"
	"gopkg.in/gorp.v1"
	_ "github.com/mattn/go-sqlite3"

	"log"
	"time"
)

func main() {
	// init dbmap
	dbmap := initDb()
	defer dbmap.Db.Close()

	// delete any existing rows
	err := dbmap.TruncateTables()
	checkErr(err, "TruncateTable failed")

	// create two posts
	p1 := newPost("Golang v1", "Version 1 of golang")
	p2 := newPost("Golang v2", "Version 2 of golang")

	// insert row - auto increment pk will be set properly after insert
	err = dbmap.Insert(&p1, &p2)
	checkErr(err, "Insert failed")

	// use convenient selectInt
	count, err := dbmap.SelectInt("SELECT COUNT(*) FROM posts")
	checkErr(err, "select count(*) failed")
	log.Println("Number of rows inserted: ", count)

	// update a row
	p2.Title = "Golang v2 changelog"
	count, err = dbmap.Update(&p2)
	checkErr(err, "Update failed")
	log.Println("Number of rows updated: ", count)

	// fetch one row - note use 'post_id' instead of Id
	err = dbmap.SelectOne(&p2, "SELECT * FROM posts where post_id=?", p2.Id)
	checkErr(err, "SelectOne failed")
	log.Println("p2 row: ", p2)

	// fetch all rows
	var posts []Post
	_, err = dbmap.Select(&posts, "SELECT * FROM posts order by post_id")
	checkErr(err, "select all failed")
	log.Println("All rows")
	for i, p := range posts {
		log.Printf("	%d: %v\n", i, p)
	}

	// delete row by pk
	count, err = dbmap.Delete(&p1)
	checkErr(err, "Delete failed")
	log.Println("Number of rows deleted: ", count)

	fetchAll(dbmap)

	// Delete row manual via Exec
	_, err = dbmap.Exec("DELETE FROM posts where post_id=?", p2.Id)
	checkErr(err, "Exec failed")

	fetchAll(dbmap)

}


// struct
type Post struct {
	// db tag lets you specify the column name if it differs from the struct field
	Id int64 `db:"post_id"`
	Created int64
	Title string `db:",size:50"` // column size set to 50
	Body string `db:"article_body,size:1024"` // set both column name and column size
}

// data structure functions
func newPost(title, body string) Post {
	return Post{
		Created: time.Now().UnixNano(),
		Title: title,
		Body: body,
	}
}

// util functions
func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(err, msg)
	}
}


// db function
func initDb() *gorp.DbMap {
	// connect to db using Go standard database/sql API
	// use whatever database/sql driver you wish depend on what database type you use
	db, err := sql.Open("sqlite3", "gorp.sqlite3")
	checkErr(err, "Sql open db fieled")

	// construct a gorp dbmap
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	// add a table, setting the table name to 'posts' and
	// specifying that the Id is an auto incrementing PK
	dbmap.AddTableWithName(Post{}, "posts").SetKeys(true, "Id")

	// create a table. in a product system this should use migration tool or create table svia script
	err = dbmap.CreateTablesIfNotExists()
	checkErr(err, "create table failed")

	return dbmap

}

func fetchAll(dbmap *gorp.DbMap) {
	var posts []Post
	_, err := dbmap.Select(&posts, "SELECT * FROM posts")
	checkErr(err, "fetchall failed")
	log.Println("List all post")
	for i, p := range posts {
		log.Printf("	%d: %v\n", i, p)
	}
}