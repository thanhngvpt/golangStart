package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"fmt"
)

// struct
type TestItem struct {
	Id int
	Name string
	Phone string
}

// function
func initializeDb(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)
	if err != nil {
		log.Fatal(err)
	}

	if db == nil {
		panic(err)
	}

	return db
}

func createTable(db *sql.DB) {
	// create table if not exist
	sqlTable := `
		CREATE TABLE IF NOT EXISTS items(
			Id TEXT NOT NULL PRIMARY KEY,
			Name TEXT,
			Phone TEXT,
			InsertedDatetime DATETIME
		);
	`

	_, err := db.Exec(sqlTable)
	if err != nil {
		log.Fatal(err)
	}
}

func InsertItem(db *sql.DB, items []TestItem) {
	sqlInsert := `
		INSERT OR REPLACE INTO items(Id, Name, Phone, InsertedDatetime)
		VALUES (?, ?, ?, CURRENT_TIMESTAMP)
	`

	stmp, err := db.Prepare(sqlInsert)
	if err != nil {
		log.Fatal(err)
	}
	defer stmp.Close()

	for _, item := range items {
		_, err := stmp.Exec(item.Id, item.Name, item.Phone)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func readItem(db *sql.DB) []TestItem {
	sqlRead := `
		SELECT Id, Name, Phone from items
		ORDER BY datetime(InsertedDatetime) DESC
	`

	rows, err := db.Query(sqlRead)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var result []TestItem
	for rows.Next() {
		item := TestItem{}
		err := rows.Scan(&item.Id, &item.Name, &item.Phone)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, item)
	}

	return result
}

// main
func main() {
	// create constant contain path to db
	const dbName = "db.sqlite3"

	// init db -  create a db
	db := initDb(dbName)
	defer db.Close()

	// create table in db
	createTable(db)

	// create a slice of TestItem to save data
	items := []TestItem{
		TestItem{1, "John", "1234"},
		TestItem{2, "David", "43343"},
	}

	// insert data to db
	InsertItem(db, items)

	// read data from db
	item := readItem(db)

	// print data that read from db
	fmt.Println(item)

}